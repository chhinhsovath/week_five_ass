<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('blog.index');
    }

    public function about()
    {
        return view('blog.about');
    }

    public function post()
    {
        return view('blog.post');
    }

    public function contact()
    {
        return view('blog.contact');
    }
}
