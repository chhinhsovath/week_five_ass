<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {    return view('welcome'); });

Route::get('/', [App\Http\Controllers\BlogController::class, 'index'])->name('index');
Route::get('index', [App\Http\Controllers\BlogController::class, 'index'])->name('index');
Route::get('about', [App\Http\Controllers\BlogController::class, 'about'])->name('about');
Route::get('post', [App\Http\Controllers\BlogController::class, 'post'])->name('post');
Route::get('contact', [App\Http\Controllers\BlogController::class, 'contact'])->name('contact');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
